1st Commit----------------------------------------------------------------------------------------------------------
Changed Title and shop 	Line 14 and Line 19
2nd Commit---------------------------------------------------------------------------------------------------------------------------
Added about.html to templates directory Line 1 -28
Added GetMapping to MainScreenController.java Line 55-59
Added link to the main page to get to the about section Line 19-28
3rd Commit---------------------------------------------------------------------------------------------------------------------------
Added parts and products only if the return from the database is greater than 5 else stock inventory is shown Line 49-130
4th Commit---------------------------------------------------------------------------------------------------------------------------
Added Buy Now button mainscreen.html Line 94
Added success page success.html Line 1-17
Added failure page failure.html Line 1-17
Made edit to Product repository to include a findOneById ProductRepository line 19-22
Added entry for a new method on the Productservice.java Line 20
Added implementation to ProductServiceImpl Line 69-84
Added getmapping for buyPage to respond with either a success message or failure message MainScreenControllerr.java 137-145
5th Commit -------------------------------------------------------------------------------------------------------------
Made changes to OutsourcedPart.java and InhousePart.java to include a minimum and maximum fields InhousePart Line 16-34 OutsourcedPart.java Line 17-35
Made modifications to MainScreenController.java to add min and max on default parts Lines 56-85
Added min and max inputs to Part.java Line 20, 33-35
Modified application.properties to change the persistent storage name Line 6
Added Labels to inhousePartForm.html and OutsourcedPartForm.html 
InhousePartForm.html Line 16, 18, 21, 23-27
OutsourcedPartForm.html line 17, 19, 25-27
Changed the about.html to reflect a different shop Line 21-25
Added new validation annotation PartInventoryValidator.java and ValidPartInventory.java
	PartInventoryValidator.java Line 1-24
	ValidPartInventory.java Line 1-20
Changed mainscreen.html to reflect the new name for the shop
	Line 14
	Line 62
Commit 6th ----------------------------------------------------------------------------------------------------------------------
Added test cases to PartTest.java
	Line 159-171



